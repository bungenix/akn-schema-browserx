#  Akoma Ntoso XML Schema Browser


A user friendly schema browser for the Akoma Ntosos XML schema. Search and Navigate elements, attributes and the relationships between various elements.
Based on the OxygenXML generated documentation. This resolves and expands groups, complex types, attribute groups etc. so you can browse the schema in terms of just elements and attributes.


## Setup 

The repo has both the front-end and the back-end components. 
Front-end is ReactJS app, back-end is an eXist-db app.

### Building the front-end

1. git clone the repo

`git clone https://gitlab.com/bungenix/akn-schema-browserx`

2. install and build the components

```
npm install
npm run build
```

3. That will create a deployable site in the  `build` subfolder

### Building the back-end

1. in the same repo that you cloned:

```
cd exist_pkg
ant xar
```

You need `Apache ant` to build this, it will generate a `xar` package that needs to be installed in eXist-db via the eXist-db dashboard

### Deploying

To deploy on Apache Http, you can use a configuration like below. 
`/home/user/dev/akn-schema-browserx/build` is the build subfolder of the front-end you built earlier.
`http://localhost:8080...` assumes that the eXist-db is listening on port 8080.

```
<VirtualHost 127.0.0.1:80>
    ProxyRequests off
    ServerName schema.local
    ServerAlias schema.local
    
        DocumentRoot "/home/user/dev/akn-schema-browserx/build"
        <Directory "/home/user/dev/akn-schema-browserx/build">
            DirectoryIndex "index.html"
            Require all granted
            AllowOverride All

            ## the following is required for client side routing to work,
            ## otherwise results in a 404 on refresh

            RewriteEngine on
            # Don't rewrite files or directories
            RewriteCond %{REQUEST_FILENAME} -f [OR]
            RewriteCond %{REQUEST_FILENAME} -d
            RewriteRule ^ - [L]
            # Rewrite everything else to index.html to allow html5 state links
            RewriteRule ^ index.html [L]
        </Directory>

    <Location "/api">
      AddType text/cache-manifest .appcache
      ProxyPass  "http://localhost:8080/exist/apps/anschema/api"
      ProxyPassReverse "http://localhost:8080/exist/apps/anschema/api/"
      ProxyPassReverseCookiePath /exist /
      SetEnv force-proxy-request-1.0 1
      SetEnv proxy-nokeepalive 1 
    </Location>


</VirtualHost>    
```