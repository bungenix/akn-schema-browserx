<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" xmlns:an="http://docs.oasis-open.org/legaldocml/ns/akn/3.0/WD17" exclude-result-prefixes="xs xd" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> May 7, 2017</xd:p>
            <xd:p><xd:b>Author:</xd:b> ashok</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    
            
    <xsl:template match="element()">
      <xsl:copy>
        <xsl:apply-templates select="@*,node()"/>
       </xsl:copy>
    </xsl:template>
    
    <xsl:template match="attribute()|text()|comment()|processing-instruction()">
      <xsl:copy/>
    </xsl:template>
    
    <xsl:template match="an:name | an:type"/>
        
     <xsl:template match="an:comment">
         <xsl:apply-templates/>
     </xsl:template>
    
</xsl:stylesheet>