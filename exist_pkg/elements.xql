xquery version "3.1";

(: 
Run this once to generate schema.xml 
Change login / password to anschema passwords
:)
import module namespace doc="http://schema.akomantoso.org/doc" at "./modules/doc.xql";
import module namespace config="http://schema.akomantoso.org/config" at "./modules/config.xqm";

declare function local:remove-namespaces($element as element()) as element() {
     element { local-name($element) } {
         for $att in $element/@*
         return
             attribute {local-name($att)} {$att},
         for $child in $element/node()
         return
             if ($child instance of element())
             then local:remove-namespaces($child)
             else $child
         }
};

declare function local:add-default-ns-node(
    $elem   as element(),
    $ns-uri as xs:string
  ) as element()
{
  element { QName($ns-uri, local-name($elem)) }{ $elem }/* 
};

let $elems := doc:elements()
let $attrs := doc:attributes()
let $cts := doc:complex-types()
let $sts := doc:simple-types()
let $egs := doc:element-groups()
let $ags := doc:attribute-groups()
let $doc_elem := config:xslt('element.xsl')
let $doc_attr := config:xslt('attribute.xsl')
let $doc_ct := config:xslt('complextype.xsl')
let $doc_st := config:xslt('simpletype.xsl')
let $doc_eg := config:xslt('element_group.xsl')
let $doc_ag := config:xslt('attribute_group.xsl')
let $doc_ns := config:xslt('schemaNS.xsl')
let $doc_schema := 
   <schema>
    <elements> {
     for $elem in $elems
          return transform:transform($elem, $doc_elem, ())
        }</elements>
    <complexTypes> {
     for $ct in $cts
          return transform:transform($ct, $doc_ct, ())        
    }</complexTypes>
    <simpleTypes> {
      for $st in $sts
        return transform:transform($st, $doc_st, ())
    }</simpleTypes>
    <elementGroups> {
        for $eg in $egs
            return transform:transform($eg, $doc_eg, ())
    } </elementGroups>
    <attributes> {
      for $attr in $attrs
          return transform:transform($attr, $doc_attr, ())
    } </attributes>
    <attributeGroups> {
      for $ag in $ags
          return transform:transform($ag, $doc_ag, ())
    } </attributeGroups>
    
   </schema>


let $login := xmldb:login($config:data-root, "admin", "")   
let $save := xmldb:store($config:data-root, "schema.xml", 
    transform:transform(
        local:remove-namespaces($doc_schema), 
        $doc_ns,
        <parameters>
            <param name="exist:stop-on-warn" value="no" />
            <param name="exist:stop-on-error" value="no" />
        </parameters>
      )
)

return $save