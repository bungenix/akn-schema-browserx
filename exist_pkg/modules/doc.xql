xquery version "3.1";

module namespace doc="http://schema.akomantoso.org/doc";
import module namespace config="http://schema.akomantoso.org/config" at "config.xqm"; 

declare namespace d="http://docbook.org/ns/docbook";

declare function doc:elements() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[normalize-space(.) = 'Element(s)']]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
};  

declare function doc:element-groups() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[normalize-space(.) = 'Element Group(s)']]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
};  

(: Attribute Group(s) :)

declare function doc:attribute-groups() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[normalize-space(.) = 'Attribute Group(s)']]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
};  



declare function doc:complex-types() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[normalize-space(.) = 'Complex Type(s)']]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
                
};  

declare function doc:simple-types() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[normalize-space(.) = 'Simple Type(s)']]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
};

declare function doc:attributes() {
    let $doc := config:data-doc()
    let $sections := $doc/d:article/d:section[./d:title[ normalize-space(.) = 'Attribute(s)' ]]/d:section
    return
        for $section in $sections 
            order by $section/@xml:id
            return
                $section
                
};  

declare function doc:element-by-name($name) {
    document {
        config:data-doc()//elements/element[@eid = $name]
    }
};