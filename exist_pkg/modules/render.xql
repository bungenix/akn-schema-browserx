xquery version "3.1";

module namespace render="http://schema.akomantoso.org/render";
import module namespace config="http://schema.akomantoso.org/config" at "config.xqm"; 
import module namespace schema="http://schema.akomantoso.org/schema" at "schema.xql";
declare namespace sx="http://schema.akomantoso.com/1.0";


declare function render:elements-usedby-complextype($forid) {
    let $ct := schema:complextype-by-name($forid)
    let $ct-usedby := $ct//sx:usedByItems
    return
        if (count($ct-usedby) gt 0) then
            for $usedby in $ct-usedby/sx:itemType/sx:item
                return
                    render:element-link($usedby/@forId, data($usedby))
        else
            ()
};


declare function render:elements-usedby-elementgroup($forid) {
    let $ct := schema:elementgroup-by-name($forid)
    let $ct-usedby := $ct//sx:usedByItems/sx:itemType
    return
        if (count($ct-usedby) gt 0) then
            render:usedby($ct-usedby)
        else
            ()
};


declare function render:element-link($elem as element()) {
        <a href="/element/{data($elem/@eid)}" data-type="element" data-item="{data($elem/@eid)}">
        &lt;{data($elem/name)}&gt;
        </a>
};


declare function render:element-link($forid, $text) {
        <a href="/element/{$forid}" data-type="element" data-item="{$forid}">
        &lt;{$text}&gt;
        </a>
};

declare function render:link-restriction-of-type($type as xs:string) {
    let $repl := normalize-space(replace($type, 'restriction of', ''))
    return
        if (starts-with($repl, 'xs:') or starts-with($repl, 'xsd:')) then
        (   
            'restriction of ',
            <a href="{render:link-xsd-type($repl)}" target="_blank">{$repl}</a>
        )
        else
            $repl
};

declare function render:link-simple-type($link-text as xs:string, $type as xs:string) {
    <a href="/simple/{$type}">{$link-text}</a>
};

declare function render:link-list-of-type($type as xs:string) {
    let $repl := normalize-space(replace($type, 'list of', ''))
    return
        if (starts-with($repl, 'xs:') or starts-with($repl, 'xsd:')) then
            <a href="{render:link-xsd-type($repl)}" target="_blank">{$type}</a>
        else
            render:link-simple-type($type, $repl)
};


declare function render:link-union-of-type($type as xs:string) {
    let $repl := replace(normalize-space(replace($type, 'union of', '')), '[\(\)]', '')
    let $indivs := tokenize($repl, ",")
    let $count-indivs := count($indivs)
    return
     (
        "union of(",
        for $indiv at $pos in $indivs
           return
           (
            render:type-link-negotiator($indiv),
            if ($pos eq $count-indivs) then "" else ","
            )
        ,
        ")"
     )
};




declare function render:link-xsd-type($type as xs:string) {
  let $tmpl := "http://www.datypic.com/sc/xsd/t-XSD_TYPE.html"
  let $repl :=
    if (starts-with($type, "xs:")) then
        replace(
            replace($type, "xs:", "xsd:"),
            ":",
            "_"
        )
    else
        replace($type, ':', "_")

  return replace($tmpl, 'XSD_TYPE', $repl)
};


declare function render:type-link-negotiator($input-type as xs:string) {
    let $dtype := normalize-space($input-type)
    return
         if (
            starts-with($dtype, "xsd:") or 
            starts-with($dtype, 'xs:')
         ) then
            <a target="_blank" href="{render:link-xsd-type($dtype)}">
               {$dtype}
            </a>
         else
         if (
            starts-with($dtype, 'restriction of')
          ) then 
            render:link-restriction-of-type($dtype)
         else
         if (
            starts-with($dtype, 'list of')
         ) then
            render:link-list-of-type($dtype)
         else
         if (
            starts-with($dtype, 'union of')
         ) then
            render:link-union-of-type($dtype)
         else
            render:link-simple-type($dtype, $dtype)
         (:
         if ($simple-type) then 
           <a href="index.html?type=simple&amp;item={$dtype}">
               {$dtype}
            </a>
         else
           $dtype:)
};

declare function render:element-attributes($p-doc, $attrs) {
    let $s-doc := schema:doc()
    for $attr at $pos in $attrs
        let $attr-id := replace(replace(data($attr/@link),'\s+$',''),'^\s+','')
        let $attr-doc := $s-doc//sx:attributes/sx:attribute/@eid[lower-case(.) eq lower-case($attr-id)]/parent::sx:attribute
        let $simple-type := $attr-doc/sx:properties/sx:property[@name = 'content']/@value eq 'simple'
        let $facets := $attr-doc/sx:facets/sx:facet[@name = 'enumeration']/sx:value
        let $attr-desc := $attr-doc/sx:desc
        return
              <table width="94%" class="{ concat('attrtable', $pos mod 2) }">
                <tr>
                 <td colspan="4" width="100%">
                    <div class="attrname">
                    
                    @{
                        if (contains($attr-id, '_')) then 
                            tokenize($attr-id, '_')[2]
                        else
                            $attr-id
                     }
                     
                    </div>
                    {
                        if (count($attr-desc) gt 0) then 
                           <div class="serving">
                               {$attr-desc}                          
                           </div>
                        else
                            ()
                    }
                    <div class="hrline"></div>
                 </td>
                </tr>
                 <tr>
                     <td colspan="2"  width="50%">
                        <div class="attritemlabel">
                        Type
                        </div>
                     </td>
                     <td colspan="2"  width="50%">
                       <div class="attritem">
                        {
                         let $dtype := 
                            data($attr-doc/sx:type)
                          return
                             render:type-link-negotiator($dtype)
                            (:
                             if (
                                starts-with($dtype, "xsd:") or 
                                starts-with($dtype, 'xs:')
                             ) then
                                <a target="_blank" href="{render:link-xsd-type($dtype)}">
                                   {$dtype}
                                </a>
                             else
                             if (
                                starts-with($dtype, 'restriction of')
                              ) then 
                                render:link-restriction-of-type($dtype)
                             else
                             if (
                                starts-with($dtype, 'list of')
                             ) then
                                render:link-list-of-type($dtype)
                             else
                             if ($simple-type) then 
                               <a href="index.html?type=simple&amp;item={data($attr-doc/type)}">
                                   {$dtype}
                                </a>
                             else
                               $dtype
                               :)
                          }
                        </div>
                     </td>
                </tr>
                
                {
                if (count($attr-desc) gt 0) then 
                    <tr>
                        <td colspan="2">
                            <div class="attritemlabel">
                            
                            </div>
                        </td>
                        <td colspan="2">
                        </td>
                    </tr>
                else
                    ()
                
                
                }
                {
                if (count($facets) gt 0) then 
                <tr>
                  <td colspan="2">
                   <div class="attritemlabel">
                    Possible Values
                    </div>
                  </td>
                  <td colspan="2">
                   <div class="attritemvalues">
                    {
                     for $facet at $pos in $facets
                      return
                        if ($pos eq count($facets)) then
                           data($facet)
                        else
                           data($facet) || ", "
                    }
                    </div>
                  </td>
                </tr>
                else 
                    ()
                }  
            </table>

};

declare function render:usedby($usedbys) {
    for $usedby in $usedbys
      return
        if ($usedby/@type eq 'Element') then
             for $item in $usedby/sx:item
                return
                    render:element-link($item/@forId, data($item))
        else
        if (data($usedby/@type) eq 'ComplexType') then
            for $item in $usedby/sx:item
                return
                    render:elements-usedby-complextype($item/@forId)
        else
        if (data($usedby/@type) eq 'ElementGroup') then
            for $item in $usedby/sx:item
                return
                    render:elements-usedby-elementgroup($item/@forId)
        else
            ()
            
};

declare function render:element-links($items, $item-id) {
 let $items-count := count($items)
 let $ord-items :=  
    for $new-ub-item in $items
        order by data($new-ub-item)
        return $new-ub-item
 return
    for $new-ub-item at $pos in $ord-items
        order by data($new-ub-item)
        return
            if ($new-ub-item/@data-item eq $item-id) then
                ()
            else
                (
                    $new-ub-item, 
                    if ($items-count eq $pos) then "" else ", " 
                )
};


declare function render:simpleType($elem) {
    let $p-doc := fn:root($elem)
    let $item-id := data($elem/@eid)
    let $facets := $elem/sx:facets/sx:facet[@name = 'enumeration']/sx:value
    return
      <div id="elements-table">
        <table>
        <tr>
            <td colspan="2" class="header">SimpleType {data($elem/@eid)} <br />
            <small style="font-size:11px;">(This is an abstraction) </small>
            </td>
        </tr>
        <tr>
            <td colspan="2" ><div class="serving"> {$elem/sx:desc/child::node()}  </div></td>
        </tr>
        <tr style="height: 7px">
            <td bgcolor="#000000" colspan="2"></td>
         </tr>
          {
            if (count($facets) gt 0) then 
            (
            <tr>
              <td colspan="2">
               <div class="headline">
                Possible Values
                </div>
              </td>
             </tr>,
             <tr>
              <td colspan="2">
               <div class="serving">
                {
                 for $facet at $pos in $facets
                  return
                    if ($pos eq count($facets)) then
                       data($facet)
                    else
                       data($facet) || ", "
                }
                </div>
              </td>
            </tr>)
            else 
                ()
        }           
        <tr>
            <td colspan="2">
            <div class="imgholder">
               <img src="{ concat("/data/" , $elem/sx:img/@src) }"  />
               </div>
            </td>
        </tr>
        
        
        
        </table>
      </div>
};

declare function render:element($elem) {
    let $p-doc := fn:root($elem)
    let $item-id := data($elem/@eid)
    return
      <div id="elements-table">
    <table>
        <tr>
            <td colspan="2" class="header">Element &lt;{data($elem/@eid)}&gt; </td>
        </tr>
        <tr>
            <td colspan="2" ><div class="serving"> {data($elem/sx:desc)}  </div></td>
        </tr>
         <tr style="height: 7px">
            <td bgcolor="#000000" colspan="2"></td>
        </tr>
        <!-- used by items -->
        {
        let $usedbys := $elem//sx:usedByItems/sx:itemType
        return
         if (count($usedbys) gt 0) then
                (
                <tr>
                   <td colspan="2">
                    <div class="headline">
                        Used by 
                    </div>
                   </td>
                </tr>,
                <tr>
                   <td colspan="2">
                      <div class="serving">
                        {
                         let $ub-items := render:usedby($usedbys)
                         let $ub-items-data := distinct-values($ub-items/@data-item)
                         let $new-ub-items := 
                            for $data in $ub-items-data
                                return $ub-items[@data-item = $data][1]
                         
                         return render:element-links($new-ub-items, $item-id)
                        }
                      </div>
                   </td>
                </tr>
                )
         else
            ()
        }
        <!-- child elements -->
        {
        let $childelems := $elem//sx:childElements/sx:childElement
        return
            if (count($childelems) gt 0 ) then 
                (
                <tr>
                    <td colspan="2"><div class="headline">Child Elements</div> </td>
                </tr>,
                <tr>
                    <td colspan="2">
                        <div class="serving">
                        {
                         let $child-links := 
                            for $celem in $childelems
                                return
                                    render:element-link($celem/@forId, data($celem))
                         return
                            render:element-links($child-links, $item-id)
                        }
                        </div>
                    </td>
                </tr>
                )
            else
                ()
        }
        <!-- available attributes -->
        
        {
        let $attrs := $elem//sx:hasAttributes/sx:hasAttribute
        return
        if (count($attrs) gt 0) then 
           (
            <tr>
                <td colspan="2"><div class="headline">Attributes</div> </td>
            </tr>,
            <tr>
                <td colspan="2">
                    <!-- rendernig attrs -->
                    {
                       render:element-attributes($p-doc, $attrs) 
                    }
                </td>
            </tr>
            )
        else 
            () 
         
        } 
        <tr>
                <td colspan="2"><div class="headline">Diagram</div> </td>
            </tr>
        <tr>
            <td colspan="2">
              <div class="imgholder">
               <img src="{ concat("/data/" , $elem/sx:img/@src) }"  />
              </div>
            </td>
        </tr>
    </table>
    </div>
};