xquery version "3.1";

module namespace schema="http://schema.akomantoso.org/schema";
import module namespace config="http://schema.akomantoso.org/config" at "config.xqm"; 
declare namespace sx="http://schema.akomantoso.com/1.0";

declare function schema:doc() {
    doc($config:data-root || "/schema.xml")
};

declare function schema:element-by-name($name) {
    let $doc := schema:doc()
    return $doc//sx:elements/sx:element[lower-case(@eid) = lower-case($name)] 
};


declare function schema:simpletype-by-name($name) {
    let $doc := schema:doc()
    return $doc//sx:simpleTypes/sx:simpleType[lower-case(@eid) = lower-case($name)]
};


declare function schema:complextype-by-name($name) {
    let $doc := schema:doc()
    return $doc//sx:complexTypes/sx:complexType[lower-case(@eid) = lower-case($name)]
};

declare function schema:elementgroup-by-name($name) {
    let $doc := schema:doc()
    return $doc//sx:elementGroups/sx:elementGroup[lower-case(@eid) = lower-case($name)]
};


declare function schema:random-elements() {
    let $doc := schema:doc()
    let $elems := $doc//sx:elements/sx:element
    let $max := count($elems)
    let $rand-series := 
        for $n in (1 to 5)
            let $r := util:random($max)
            return
            if ($r eq 0) then
                1
            else
                $r
    return
        for $n in $rand-series
            return
                $elems[$n]
};

