xquery version "3.1";

module namespace utils="http://schema.akomantoso.org/utils";

import module namespace config="http://schema.akomantoso.org/config" at "config.xqm";



declare function utils:file-from-uri($uri) {
    tokenize($uri, '/')[last()]
};


declare function utils:file-prefix($file) {
    tokenize($file, '\.')[1]
};

declare function utils:request-url-prefix() {
    utils:file-prefix(
        utils:file-from-uri(request:get-uri())
    )
};



declare function utils:generate-filename($uri) {
    substring(
        replace(
            replace($uri, "/", "_"), 
            "@", 
            "_at_"
        ),
        2) || ".xml"          
};

declare function utils:remove-namespaces($element as element()) as element() {
     element { local-name($element) } {
         for $att in $element/@*
         return
             attribute {local-name($att)} {$att},
         for $child in $element/node()
         return
             if ($child instance of element())
             then utils:remove-namespaces($child)
             else $child
         }
};


declare function utils:rep-state($state) {
    let $rep-states := ("changes_accepted", "complete" )
    let $found := index-of($rep-states, $state) 
    return
        if ($found) then
            true()
        else
            false()
};

declare function utils:rep-num($state, $num) {
     if (utils:rep-state($state)) then
        replace($num, "DC", "REP")
     else
        $num 
};


declare
function local:stringdate-as-dateTime($string-date as xs:string) as xs:dateTime {
    let $as-datetime := fn:dateTime(
        xs:date($string-date), 
        xs:time("00:00:00")
       )
    return $as-datetime
};


declare function utils:http-header-date() {
  let $d:= adjust-dateTime-to-timezone(current-dateTime(), xs:dayTimeDuration("PT0H"))
  (: Fri, 30 Oct 1998 14:19:41 GMT :)
  return format-dateTime($d,'[FNn,*-3], [D01] [MNn,*-3] [Y0001] [H01]:[m01]:[s01] GMT', (), (), 'uk')
};

declare function
utils:fo-to-pdf-stream($fo-doc, $file-name) {
    response:stream-binary(
        utils:fo-to-pdf($fo-doc), 
        "application/pdf", 
        $file-name
        )
};

declare function
utils:fo-to-pdf($fo-doc) {
    let $data := httpclient:post(
            xs:anyURI("http://localhost/pAnxmlToPdf/Convert"), 
            $fo-doc, 
            false(), 
            ()
         )
    return 
        data($data/httpclient:body)
};


