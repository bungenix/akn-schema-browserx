xquery version "3.1";

import module namespace config="http://schema.akomantoso.org/config" at "../modules/config.xqm";

let $schema-doc := doc($config:data-root || "/schema/akomantoso30.xsd")
 return 
    response:stream-binary(
        util:string-to-binary(
            util:serialize(
                $schema-doc, 
                'method=xml'
                ),
            'UTF-8'
            ),
        "application/octet-stream", 
        "akomantoso30.xsd"
       )
