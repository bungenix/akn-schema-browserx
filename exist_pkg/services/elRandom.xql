xquery version "3.1";

import module namespace utils="http://schema.akomantoso.org/utils" at "../modules/utils.xql";
import module namespace schema="http://schema.akomantoso.org/schema" at "../modules/schema.xql";

declare namespace sx="http://schema.akomantoso.com/1.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";

let $elems := schema:random-elements()
return $elems/sx:name/text()

