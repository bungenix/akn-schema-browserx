xquery version "3.1";

import module namespace utils="http://schema.akomantoso.org/utils" at "../modules/utils.xql";
import module namespace schema="http://schema.akomantoso.org/schema" at "../modules/schema.xql";
import module namespace render="http://schema.akomantoso.org/render" at "../modules/render.xql";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "xml";
let $element := request:get-parameter("el", '')

let $element-doc :=  schema:simpletype-by-name($element)
return render:simpleType(
      $element-doc
    )