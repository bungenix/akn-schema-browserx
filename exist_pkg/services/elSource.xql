xquery version "3.1";

import module namespace utils="http://schema.akomantoso.org/utils" at "../modules/utils.xql";
import module namespace schema="http://schema.akomantoso.org/schema" at "../modules/schema.xql";
declare namespace sx="http://schema.akomantoso.com/1.0";

declare namespace output="http://www.w3.org/2010/xslt-xquery-serialization";
declare option output:method "json";
declare option output:media-type "application/json";


declare function local:elements-map($elem) {
    map {
        "name":= data($elem/sx:name),
        "link":= data($elem/@eid),
        "desc":= data($elem/sx:desc)
    }
};

declare function local:elements($terms) {
    for $elem in schema:doc()//sx:elements/sx:element/@eid[starts-with(lower-case(.), lower-case($terms))]/parent::sx:element
        order by $elem/@eid
        return local:elements-map($elem)
};

let $terms := request:get-parameter("term", '')

return
    if ($terms eq '') then
        array{}
    else
        array{local:elements($terms)}

