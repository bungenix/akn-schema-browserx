<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:d="http://docbook.org/ns/docbook" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs xd" version="2.0">
    <xsl:import href="common.xsl"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b> May 7, 2017</xd:p>
            <xd:p>
                <xd:b>Author:</xd:b> ashok</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:apply-templates mode="rows"/>
    </xsl:template>    
    
    <xsl:template match="d:section" mode="rows">
       <attribute eid="{@xml:id}">
            <xsl:call-template name="rows"/>
        </attribute>
    </xsl:template>
    
    <xsl:template name="rows">
            <xsl:for-each select="./d:informaltable/d:tgroup/d:tbody/d:row">
                <xsl:variable name="entry_val" select="normalize-space(./d:entry[1])"/>
                <xsl:choose>
                    <xsl:when test="$entry_val eq 'Type'">
                        <xsl:call-template name="attr_type"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Annotations'">
                        <xsl:call-template name="render_desc"/>
                    </xsl:when>                    
                    <xsl:when test="$entry_val eq 'Properties'">
                        <xsl:call-template name="render_properties"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Facets'">
                        <xsl:call-template name="render_facets"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Used by'">
                        <xsl:call-template name="render_usedby"/>
                    </xsl:when>
                    
                    <xsl:otherwise>
                        <xsl:comment>IGNORE (<xsl:value-of select="."/>)</xsl:comment>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:for-each>                              
    </xsl:template>
    
    <xsl:template name="attr_type">
        <type>
            <xsl:value-of select="./d:entry[2]"/>
        </type>
    </xsl:template>





  
    
</xsl:stylesheet>