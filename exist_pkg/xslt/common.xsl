<xsl:stylesheet xmlns:xh="http://www.w3.org/1999/xhtml" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:d="http://docbook.org/ns/docbook" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs xd" version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b> May 9, 2017</xd:p>
            <xd:p>
                <xd:b>Author:</xd:b> ashok</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    
    <xsl:template name="render_type">
        <xsl:variable name="type_val" select="normalize-space(d:entry[2])"/>
        <type>
            <xsl:choose>
                <xsl:when test="starts-with($type_val, 'extension of')">
                    <xsl:attribute name="extends">
                        <xsl:value-of select="./d:link/@linkend"/>
                    </xsl:attribute>
                </xsl:when>
            </xsl:choose>
        </type>
    </xsl:template>
            
    <xsl:template name="render_diag">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="./d:entry[2]//d:imageobject/d:imagedata/@fileref"/>
            </xsl:attribute>
        </img>
    </xsl:template>        
    
    <xsl:template name="render_properties">
        <properties>
            <xsl:variable name="prop_row" select="./d:entry[2]//d:informaltable//d:tbody/d:row"/>
            <xsl:for-each select="$prop_row">
                <property name="{normalize-space(replace(./d:entry[1], ':', ''))}" value="{normalize-space(./d:entry[2])}"/>
            </xsl:for-each>
        </properties>
    </xsl:template>        


    

    
      <xsl:template name="render_model">
          <xsl:if test="count(./d:entry[2]/d:link) gt 0">
              <childElements>
              <xsl:for-each select="./d:entry[2]/d:link">
                  <childElement forId="{replace(@linkend, 'http___docs.oasis-open.org_legaldocml_ns_akn_3.0_WD17_', '')}">
                      <xsl:choose>
                          <xsl:when test="contains(., '{')">
                            <xsl:variable name="rule">
                                 <xsl:analyze-string select="." regex="[A-Za-z]+([{{]?[0-9]?[,]?[0-9]?[}}]?)?">
                                    <xsl:matching-substring>
                                        <xsl:value-of select="regex-group(1)"/>
                                    </xsl:matching-substring>
                                </xsl:analyze-string>
                            </xsl:variable>
                            <xsl:variable name="rule_dec" select="tokenize(replace($rule, '[{{}}]', ''), ',')"/>
                            <xsl:if test="count($rule_dec) eq 1">
                                <xsl:attribute name="minOccurs" select="$rule_dec[1]"/>
                            </xsl:if>
                            <xsl:if test="count($rule_dec) eq 2">
                                <xsl:attribute name="minOccurs" select="$rule_dec[1]"/>
                                <xsl:attribute name="maxOccurs" select="$rule_dec[2]"/>
                            </xsl:if>
                            <xsl:value-of select="substring-before(., '{')"/>
                          </xsl:when>
                          <xsl:when test="contains(., '+')">
                              <xsl:attribute name="minOccurs" select="'1'"/>
                              <xsl:value-of select="replace(., '\+', '')"/>
                          </xsl:when>
                          <xsl:when test="contains(., '*')">
                              <xsl:attribute name="minOccurs" select="'1'"/>
                              <xsl:value-of select="replace(., '\*', '')"/>
                          </xsl:when>
                          <xsl:otherwise>
                              <xsl:value-of select="."/>
                          </xsl:otherwise>
                      </xsl:choose>
                  </childElement>
              </xsl:for-each>
          </childElements>
         </xsl:if>
      </xsl:template>
   
    
    
     <xsl:template name="render_attributes">
         <xsl:variable name="attrs" select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row[count(d:entry) eq 3]"/>
         <xsl:if test="count($attrs) gt 0">
         <hasAttributes>
             <xsl:for-each select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row[count(d:entry) eq 3]">
                 <hasAttribute>
                     <xsl:if test="./d:entry[1]//d:link/@linkend">
                         <xsl:attribute name="link">
                             <xsl:value-of select="./d:entry[1]//d:link/@linkend"/>
                         </xsl:attribute>    
                         <name>
                             <xsl:value-of select="./d:entry[1]//d:link"/>
                         </name>                         
                     </xsl:if>
                     <type>
                         <xsl:if test="./d:entry[2]//d:link/@linkend">
                             <xsl:attribute name="link">
                                 <xsl:value-of select="./d:entry[2]//d:link/@linkend"/>
                             </xsl:attribute>
                         </xsl:if>
                         <xsl:value-of select="./d:entry[2]"/>
                     </type>
                     <usage>
                         <xsl:value-of select="./d:entry[3]"/>
                     </usage>
                 </hasAttribute>
             </xsl:for-each>
         </hasAttributes>
         </xsl:if>
      </xsl:template>
    
    
    <xsl:template name="render_usedby">
        <xsl:variable name="usedby" select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row"/>
        <xsl:if test="count($usedby) gt 0">
            <usedByItems>
            <xsl:for-each select="$usedby">
                <xsl:variable name="entry_type" select="replace(normalize-space(d:entry[1]), '\s+', '')"/>
                <xsl:variable name="entry_type_normalized">
                    <xsl:choose>
                        <xsl:when test="ends-with($entry_type, 's')">
                            <xsl:value-of select="substring($entry_type, 1, string-length($entry_type) - 1)"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$entry_type"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <itemType type="{$entry_type_normalized}">
                 <xsl:for-each select="d:entry[2]/d:link">
                     <item forId="{@linkend}">
                                <xsl:value-of select="."/>
                            </item>           
                 </xsl:for-each>
                </itemType>
            </xsl:for-each>
            </usedByItems>
        </xsl:if>
    </xsl:template>
    
    
   <xsl:template name="render_desc">
        <desc>
            <xsl:choose>
                <xsl:when test="count(./d:entry[2]/d:para) gt 0">
                    <xsl:comment>child para</xsl:comment>
                    <xsl:copy-of select="./d:entry[2]/d:para/child::node()"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:comment>no child para</xsl:comment>
                    <xsl:copy-of select="./d:entry[2]/child::node()"/>
                </xsl:otherwise>
            </xsl:choose>
        </desc>
    </xsl:template>
    
    
    
    <xsl:template name="render_facets">
        <facets>
            <xsl:for-each-group select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row" group-by="d:entry[1]">
                <facet name="{current-grouping-key()}">
                <xsl:for-each select="current-group()">
                    <value>
                            <xsl:value-of select="d:entry[2]"/>
                        </value>
                </xsl:for-each>
                </facet>                    
            </xsl:for-each-group>
        </facets>
    </xsl:template>    
        
    
</xsl:stylesheet>