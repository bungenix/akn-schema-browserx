<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs xd" version="2.0">


<xsl:template match="/">
    <xsl:apply-templates/>
</xsl:template>

<xsl:template match="element">
    <table>
        <tr>
            <td>Name</td>
            <td><xsl:value-of select="./name"/></td>
        </tr>
        <tr>
            <td>OASIS Description</td>
            <td><xsl:value-of select="./desc"/></td>
        </tr>
        <tr>
            <td>Attributes</td>
            <td>
                <xsl:for-each select="./hasAttributes/hasAttribute">
                    <table>
                        <tr>
                            
                        </tr>
                        <tr>
                            
                        </tr>
                    </table>
                </xsl:for-each>
                
                
            </td>
        </tr>
                
    </table>
</xsl:template>


</xsl:stylesheet>