<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:d="http://docbook.org/ns/docbook" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" exclude-result-prefixes="xs xd" version="2.0">
   <xsl:import href="common.xsl"/>
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p>
                <xd:b>Created on:</xd:b> May 7, 2017</xd:p>
            <xd:p>
                <xd:b>Author:</xd:b> ashok</xd:p>
            <xd:p/>
        </xd:desc>
    </xd:doc>
    <xsl:template match="/">
        <xsl:apply-templates mode="rows"/>
    </xsl:template>    
    
    <xsl:template match="d:section" mode="rows">
       <element eid="{@xml:id}">
            <name>
                <xsl:value-of select="./d:title[1]/d:literal"/>
            </name>
            <xsl:call-template name="rows"/>
        </element>
    </xsl:template>
    
    <xsl:template name="rows">
            <xsl:for-each select="./d:informaltable/d:tgroup/d:tbody/d:row">
                <xsl:variable name="entry_val" select="normalize-space(./d:entry[1])"/>
                <xsl:choose>
                    <xsl:when test="$entry_val eq 'Annotations'">
                        <xsl:call-template name="render_desc"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Diagram'">
                        <xsl:call-template name="render_diag"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Type'">
                        <xsl:call-template name="elem_type"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Type hierarchy'">
                        <xsl:call-template name="elem_typehier"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Properties'">
                        <xsl:call-template name="elem_properties"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Used by'">
                        <!-- used by -->
                        <xsl:call-template name="render_usedby"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Model'">
                        <xsl:call-template name="render_model"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Children'">
                        <xsl:call-template name="elem_children"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Instance'">
                        <xsl:call-template name="elem_instance"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Attributes'">
                        <xsl:call-template name="render_attributes"/>
                    </xsl:when>
                    <xsl:when test="$entry_val eq 'Source'">
                        <xsl:call-template name="elem_source"/>
                    </xsl:when>
                </xsl:choose>
            </xsl:for-each>                              
    </xsl:template>
    
    <!--
    <xsl:template name="elem_desc">
        <desc>
            <xsl:value-of select="./d:entry[2]"/>
        </desc>
    </xsl:template>
        -->
        
        <!--
    <xsl:template name="elem_diag">
        <img>
            <xsl:attribute name="src">
                <xsl:value-of select="./d:entry[2]//d:imageobject/d:imagedata/@fileref"/>
            </xsl:attribute>
        </img>
    </xsl:template>        
    -->
    
    <xsl:template name="elem_type"/>
    <xsl:template name="elem_typehier"/>
    <xsl:template name="elem_properties"/>
    
    <xsl:template name="elem_usedby">
        <xsl:variable name="usedby" select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row"/>
        <xsl:if test="count($usedby) gt 0">
            <usedByItems>
            <xsl:for-each select="$usedby">
                <xsl:variable name="entry_type" select="replace(normalize-space(d:entry[1]), '\s+', '')"/>
                <itemType type="{$entry_type}">
                 <xsl:for-each select="d:entry[2]/d:link">
                     <item forId="{@linkend}">
                                <xsl:value-of select="."/>
                            </item>           
                 </xsl:for-each>
                </itemType>
            </xsl:for-each>
            </usedByItems>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="elem_children"/>
     
    <xsl:template name="elem_instance"/>
    
    <!--
     <xsl:template name="elem_attributes">
         <xsl:variable name="attrs" select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row[count(d:entry) eq 3]"/>
         <xsl:if test="count($attrs) gt 0">
         <hasAttributes>
             <xsl:for-each select="./d:entry[2]/d:informaltable/d:tgroup/d:tbody/d:row[count(d:entry) eq 3]">
                 <hasAttribute>
                     <xsl:if test="./d:entry[1]//d:link/@linkend">
                         <xsl:attribute name="link">
                             <xsl:value-of select="./d:entry[1]//d:link/@linkend"/>
                         </xsl:attribute>    
                         <name>
                             <xsl:value-of select="./d:entry[1]//d:link"/>
                         </name>                         
                     </xsl:if>
                     <type>
                         <xsl:if test="./d:entry[2]//d:link/@linkend">
                             <xsl:attribute name="link">
                                 <xsl:value-of select="./d:entry[2]//d:link/@linkend"/>
                             </xsl:attribute>
                         </xsl:if>
                         <xsl:value-of select="./d:entry[2]"/>
                     </type>
                     <usage>
                         <xsl:value-of select="./d:entry[3]"/>
                     </usage>
                 </hasAttribute>
             </xsl:for-each>
         </hasAttributes>
         </xsl:if>
      </xsl:template>
      -->
      
      
      <xsl:template name="elem_source"/>
    
</xsl:stylesheet>