import React, {useEffect} from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import SchemaHome from './SchemaApp';

function App() {
  useEffect( () => {
    document.title = "AkomaNtoso Schema Browser";
  })
  return (
   <Router>
    <Switch>
    <Route exact path="/:type/:element" component={SchemaHome} />
    <Route path="/" component={SchemaHome} />
    </Switch>
   </Router>
  );
}

export default App;
