import React from 'react';
import Autosuggest from 'react-autosuggest';
import { apiElem, apiElemFull, apiElemSimple, apiElemRandom } from './utils/apihelper';

import './scss/AutoSuggest.scss';
import './scss/SearchResults.scss';

class ElementSelect extends React.Component {

    constructor(props) {
        super(props);
        this.lastRequestId = null;
        this.state = {
            value: '',
            loading: false,
            suggestions: [],
            schemaData: '',
            randomData: []
        };
    }

    
    async componentDidMount() {
        const {type, element} = this.props;
        if (element !== undefined) {
            this.fetchElementData(type, element);
        } else {
            this.fetchRandomData();
        }
    }


    async componentDidUpdate(prevProps) {
        if (prevProps.element !== this.props.element) {
            const {element, type} = this.props;
            if (element !== undefined) {
                this.fetchElementData(type, element);
            } else {
                this.fetchRandomData();
            }

            
        }
    }

    async fetchElementData(type, element) {
        let responseData = type === "element" ? await apiElemFull(element) : await apiElemSimple(element);
        this.setState({
            schemaData: responseData
        });
        document.title =  type === "element" ? `AkomaNtoso element <${element}>` : `AkomaNtoso simpleType ${element}` ;
    }

    async fetchRandomData() {
        let responseData = await apiElemRandom();
        this.setState({
            randomData: responseData
        });
    }

    getSuggestionsAsync = (value) => {
        if (this.lastRequestId !== null ) {
            clearTimeout(this.lastRequestId);
        }
        this.setState({
            loading: true
        });
        this.lastRequestId = setTimeout(
            () => this.getSearchResultsAsync(value)
            ,
            1000
        );
    };

    onSuggestionsFetchRequested = ({ value }) => this.getSuggestionsAsync(value);

    shouldRenderSuggestions = (value) => value.trim().length > 0;
    
    renderSuggestion = (suggestion) => 
        <div className="ui-ac-item-render">
            <div className="ui-ac-item-render-content">
                <span style={{color: "orange", fontSize: "18px"}}>{suggestion.name}</span>
                <div style={{ fontStyle: "italic", fontSize: "13px"}}>
                    {suggestion.desc}
                </div>
            </div>
        </div>;
    
    
    onSuggestionsClearRequested = () => 
        this.setState({
                suggestions: []
        });

    getSearchResultsAsync = async (value) => {
        const inputValue = value.trim().toLowerCase();
        let responseData = await apiElem(inputValue);
        this.setState({
            loading: false,
            suggestions: responseData
        });
    };    


    getSuggestionValue = (suggestion) => suggestion.name;

    onChange = (event, { newValue }) => {
        this.setState({
          value: newValue
        });
      };

    /**
    * We render a custom input component to pass in the data-loading prop which is
    * used to set the css spinner
    */
   renderInputComponent = inputProps => 
            <input data-loading={ inputProps.loading } {...inputProps} />;

    onSuggestionSelected = async (event, data) => {
        const {suggestion} = data;
        const {name} = suggestion;
        const { history } = this.props;
        const newRoute = '/element/' + name ;
        history.push(newRoute);
    }    


    renderRandomLinks = (linksArr) => {
        const links = linksArr.map( link => {
            return `<a href="/element/${link}">&lt;${link}&gt;</a>`;
        });
        return links.join(",");
    }

    render() {
        const { value, suggestions, loading, schemaData, randomData} = this.state;
        const { element } = this.props;
        const inputProps = {
          placeholder: "Start typing an element name here",
          height: "30px",
          autoFocus:true,
          value,
          loading: loading ? "yes" : "no",
          onChange: this.onChange
        };
        return (
            <div className="search-container">
            <Autosuggest 
                suggestions={suggestions}
                onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                onSuggestionSelected={this.onSuggestionSelected}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                shouldRenderSuggestions={this.shouldRenderSuggestions}
                renderInputComponent={this.renderInputComponent}
                inputProps={inputProps} />
                {
                    element === undefined ?
                    <div id="random-links">or start here <a href="/element/akomaNtoso">&lt;akomaNtoso&gt;</a> or lookup some random elements : 
                    <span dangerouslySetInnerHTML={{__html: this.renderRandomLinks(randomData)}} />... </div>:
                    <div id="search-results" 
                    dangerouslySetInnerHTML={{__html: schemaData}} />                     
                }
            </div>
          );
    }

};

export default ElementSelect;