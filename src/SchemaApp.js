import React from 'react';
import Avatar from '@material-ui/core/Avatar';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';

import CssBaseline from '@material-ui/core/CssBaseline';

import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Link from '@material-ui/core/Link';

import ButtonGroup from '@material-ui/core/ButtonGroup';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import ElementSelect from './ElementSelect';

import SearchIcon from '@material-ui/icons/Search';
import MenuIcon from '@material-ui/icons/Menu';
import TwitterIcon from '@material-ui/icons/Twitter';

import './scss/SchemaApp.scss';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link  href="https://www.bungeni.com/">
        Bungeni.com
      </Link>{' '}
      {new Date().getFullYear()}
      {'. '}
      <Link  href="https://gitlab.com/bungenix/akn-schema-browserx">Source code @gitlab</Link>
      {' '} Generated from auto-generated <Link  target="_blank" href="https://www.oxygenxml.com/">Oxygen XML</Link> documentation of the <Link  href="/data/schema/akomantoso30.xsd" target="_blank">AKN schema</Link> .
    </Typography>
  );
}

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor:  "#5fcf80" 
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    color: "#5fcf80",
    fontSize: "32px",
    textTransform: "uppercase",
    fontWeight: "bold"
  },  
  menuLinks: {
    flexGrow: 1,
    color: "#5fcf80",
     fontSize: "18px",
     textAlign: "right"
  }
}));


export const preventDefault = event => event.preventDefault();

export const PageAppBar = ({classes}) => 
    <AppBar position="static" elevation={0} style={{backgroundColor: "white"}}>
      <Toolbar>
        <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
          <MenuIcon style={{color: "gray"}} />
        </IconButton>
        <Typography variant="h6" className={classes.title}>
          <a className="nodec" href="/">AkomaNtoso <span style={{fontWeight: "normal"}}>Schema</span> Browser</a>
        </Typography>
        
        <ButtonGroup variant="contained" elevation={0} size="small" aria-label="small contained button group">
        <Button href="https://twitter.com/ashokharix"><TwitterIcon /></Button>
        <Button  href="https://www.bungeni.com" >Bungeni.com</Button>
        <Button  href="https://www.akomantoso.io" >AkomaNtoso.io</Button>
        </ButtonGroup>
      </Toolbar>
  </AppBar>
;


export default function SchemaHome(props) {
  const classes = useStyles();
  const {match, history} = props;
  const {params} = match;
  return (
    <Container component="main" maxWidth="xl" style={{backgroundColor: "white"}}>
      <CssBaseline />
      <PageAppBar classes={classes} />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <SearchIcon /> 
        </Avatar><Typography component="h1" variant="h5">
          AkomaNtoso element search
        </Typography>

        <form className={classes.form} noValidate>
          <ElementSelect {...params} history={history} />
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}