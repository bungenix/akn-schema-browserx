import axios from 'axios';

export const isDevelopment = () => {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
        return true;
    } else {
        return false;
    }
};

export const  apiElem = async (input) => {
    try {
        const response = await axios.get("/api/elSource", {params: {"term": input}});
        return response.data;
    } catch (error) {
        return {"API_ERROR": error};
    }
};

export const apiElemFull = async (input) => {
    try {
        const response = await axios.get("/api/elElement", {params: {"el": input}});
        return response.data;
    } catch (error) {
        return {"API_ERROR": error};
    }
};

export const apiElemSimple = async (input) => {
    try {
        const response = await axios.get("/api/elSimple", {params: {"el": input}});
        return response.data;
    } catch (error) {
        return {"API_ERROR": error};
    }
};

export const apiElemRandom = async () => {
    try {
        const response = await axios.get("/api/elRandom");
        return response.data;
    } catch (error) {
        return {"API_ERROR": error};
    }
};